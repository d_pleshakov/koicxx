#ifndef KOICXX_FILESYSTEM_HPP
#define KOICXX_FILESYSTEM_HPP

#include <koicxx/erase_remove.hpp>
#include <koicxx/macro_magic.hpp>

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

# ifdef _MSC_VER
#  define WIN32_LEAN_AND_MEAN
#  include <Windows.h>
#  include <Urlmon.h>
# endif // _MSC_VER

#include <fstream>
#include <iterator>
#include <string>
#include <vector>

# ifdef _MSC_VER
#  pragma comment(lib, "Urlmon.lib")
# endif // _MSC_VER

namespace koicxx {
namespace filesystem {

enum class file_type
{
  TEXT
  , BINARY
};

inline
std::string get_file_content(const boost::filesystem::path& file_path, const file_type type = file_type::TEXT)
{
  std::string file_content;

  if (type == file_type::TEXT)
  {
    boost::filesystem::ifstream f(file_path);
    if (f)
    {
      file_content.append(
        (std::istreambuf_iterator<char>(f))
        , std::istreambuf_iterator<char>()
      );
    }
  }
  else if (type == file_type::BINARY)
  {
    boost::filesystem::ifstream f(file_path, std::ios_base::binary | std::ios_base::ate);
    if (f)
    {
      file_content.resize(f.tellg());
      f.seekg(0, std::ios_base::beg);
      f.read(&file_content[0], file_content.size());
    }
  }

  return file_content;
}

enum class file_encoding
{
  UTF_8
};

/**
 * \brief Removes BOM from UTF8-encoded file
 * \return true on success, false otherwise
 *
 * \todo Add non-UTF8 files support
 */
inline
bool remove_bom_from_file(const boost::filesystem::path& path, file_encoding encoding)
{
  std::string all_data_from_templates_file = get_file_content(path);
  if (all_data_from_templates_file.empty())
  {
    return false;
  }

  if (encoding == file_encoding::UTF_8)
  {
    try
    {
      // Remove "truncation of constant value" warning in MSVC
      # ifdef _MSC_VER
      #  pragma warning(push)
      #  pragma warning(disable:4309)
      # endif // _MSC_VER
        
      if (all_data_from_templates_file.at(0) == static_cast<char>(0xEF) 
        && all_data_from_templates_file.at(1) == static_cast<char>(0xBB)
        && all_data_from_templates_file.at(2) == static_cast<char>(0xBF)
      )
      {
        all_data_from_templates_file.erase(0, 3);
      }
      else
      {
        return true;
      }

      # ifdef _MSC_VER
      #  pragma warning(pop)
      # endif // _MSC_VER
    }
    catch (const std::out_of_range&)
    {
      // Just ignore it (maybe empty file)
      return true;
    }
  }
  else
  {
    // Unsupported file encoding
    return false;
  }

  boost::filesystem::ofstream templates_file_out(path);
  if (!templates_file_out)
  {
    return false;
  }
  templates_file_out << all_data_from_templates_file;

  return true;
}

/**
 * \brief Returns filename without path
 */
inline
std::string get_file_name_without_path(const std::string& full_path)
{
  return boost::filesystem::path(full_path).filename().string();
}

#ifdef _MSC_VER

/**
 * \brief Returns path of the current exe
 * \return Empty boost::filesystem::path object on failure
 */
inline
boost::filesystem::path get_cur_exe_path()
{
  try
  {
    char cpath[MAX_PATH];
    const DWORD size = KOICXX_ARRAY_SIZE(cpath);
    const DWORD res = GetModuleFileNameA(NULL, cpath, size);
    if (res == size || !res)
    {
      return boost::filesystem::path();
    }

    const boost::filesystem::path path(cpath);
    return path.parent_path();
  }
  catch (const boost::filesystem::filesystem_error&)
  {
    return boost::filesystem::path();
  }
}

#endif // _MSC_VER

/**
 * \brief Returns the suggested MIME type for the given file
 * \return Suggested MIME type on success, empty string otherwise
 *
 * \todo Add support for other platforms and compilers
 */
inline
std::string get_mime_type(const boost::filesystem::path& filepath)
{
#ifdef _MSC_VER

  const boost::filesystem::path& file_extension = filepath.extension();

  LPWSTR mime_type = NULL;
  const HRESULT hr = FindMimeFromData(
    NULL                                // A pointer to the IBindCtx interface
    , file_extension.wstring().c_str()  // A pointer to a string value that contains the URL of the data
    , NULL                              // A pointer to the buffer that contains the data to be sniffed
    , 0                                 // An unsigned long integer value that contains the size of the buffer
    , NULL                              // A pointer to a string value that contains the proposed MIME type
    , FMFD_URLASFILENAME                // Treat the specified pwzUrl as a file name
    , &mime_type                        // The address of a string value that receives the suggested MIME type
    , 0);                               // Reserved. Must be set to 0
  if (SUCCEEDED(hr) && mime_type != NULL)
  {
    std::wstring mime_type_wstr(mime_type);
    std::string res(mime_type_wstr.begin(), mime_type_wstr.end());
    // Despite the documentation stating to call operator delete, the
    // returned string must be cleaned up using CoTaskMemFree
    CoTaskMemFree(mime_type);
    return res;
  }

#elif defined(__APPLE__)

  std::string file_extension = filepath.extension().string();
  // extension member function returns path contains dot, but the extensions from "mime.types" file doesn't have it
  boost::trim_left_if(file_extension, boost::is_any_of("."));

  std::ifstream mime_types_file("/etc/apache2/mime.types");
  if (mime_types_file)
  {
    for (std::string cur_line; std::getline(mime_types_file, cur_line); /* no-op */)
    {
      std::vector<std::string> mime_type_info;
      // First element is the MIME type, remain elements are extensions assoc. with this MIME type
      boost::split(mime_type_info, cur_line, boost::is_any_of("\t "), boost::token_compress_on);
      koicxx::erase_remove(mime_type_info, "");

      for (
        std::vector<std::string>::size_type i = 1, mime_type_info_size = mime_type_info.size();
        i < mime_type_info_size;
        ++i)
      {
        if (mime_type_info[i] == file_extension)
        {
          return mime_type_info[0];
        }
      }
    }
  }

#endif

  // RFC 2046 states in section 4.5.1:
  // The "octet-stream" subtype is used to indicate that a body contains arbitrary binary data.
  return "application/octet-stream";
}

} // namespace filesystem
} // namespace koicxx

#endif // !KOICXX_FILESYSTEM_HPP
